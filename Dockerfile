ARG docker_registry=forge.services.own/
ARG version=latest
FROM ${docker_registry}centos7-ownconda-runtime:$version

# Install updates
RUN yum upgrade -y

# Install Developer Toolset 7 and a newer Perl
RUN yum install -y centos-release-scl \
 && yum-config-manager --enable rhel-server-rhscl-7-rpms \
 && yum install -y devtoolset-7 \
 && yum install -y rh-perl526
#  && echo 'source /opt/rh/devtoolset-7/enable' >> /root/.bashrc

# Enable the devtoolset-6 (but not perl526) for all bash scripts
ENV BASH_ENV=/opt/rh/devtoolset-7/enable \
    ENV=/opt/rh/devtoolset-7/enable \
    PROMPT_COMMAND=". /opt/rh/devtoolset-7/enable"

# Build dependencies
RUN echo "Basic tools" \
 && yum install -y autoconf automake byacc cargo gcc gcc-c++ gcc-gfortran git \
    gperf libtool make mercurial patch patchutils perl python rust sudo \
 && echo "Basic libs" \
 && yum install -y glibc-devel glibc-headers glib2-devel libstdc++-devel \
 && echo "Xvfb for running GUI tests in headless mode" \
 && yum install -y xorg-x11-server-Xvfb mesa-dri-drivers \
 && echo "krb5" \
 && yum install -y keyutils-libs-devel \
 && echo "mariadb-connector-c" \
 && yum install -y libcurl-devel \
 && echo "qt5" \
 && yum install -y  bison flex \
    alsa-lib alsa-lib-devel cups-devel cups-libs dbus-devel dbus-libs \
    expat expat-devel jasper jasper-devel libICE libICE-devel \
    libSM libSM-devel libX11 libX11-devel libXScrnSaver libXScrnSaver-devel \
    libXcomposite libXcomposite-devel libXcursor libXcursor-devel \
    libXdamage libXdamage-devel libXext libXext-devel libXfixes libXfixes-devel \
    libXi libXi-devel libXrandr libXrandr-devel libXrender libXrender-devel \
    libXtst libXtst-devel libdrm libdrm-devel libxcb libxcb-devel \
    libxkbcommon libxkbcommon-devel libxkbcommon-x11 libxkbcommon-x11-devel \
    mesa-libEGL mesa-libEGL-devel mesa-libGL mesa-libGL-devel \
    nspr nspr-devel nss nss-devel nss-util-devel pciutils-devel pciutils-libs \
    pulseaudio-libs pulseaudio-libs-devel pulseaudio-libs-glib2

# Ownconda Python Distribution
RUN conda update --all --yes --quiet \
 && conda install conda-build own-conda-tools --yes --quiet
ADD .condarc /opt/ownconda/
